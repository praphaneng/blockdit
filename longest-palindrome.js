let s = "there";

let isPalindromic = (words) => {
    let rev = [...words].reverse().join("");
    if (words == rev) return result = {
        result: true,
        mes: words == rev,
    }
    if (words != rev) return result = {
        result: false,
        mes: "there is no palindromic in this.",
    }
}

function longestPalindrome(str1) {
    try {
        if (isPalindromic(str1).result == false) throw isPalindromic(s).mes;

        var max_length = 0,
            maxp = '';

        for (var i = 0; i < str1.length; i++) {
            var subs = str1.substr(i, str1.length);

            for (var j = subs.length; j >= 0; j--) {
                var sub_subs_str = subs.substr(0, j);
                if (sub_subs_str.length <= 1)
                    continue;

                if (isPalindromic(sub_subs_str).result == true) {
                    if (sub_subs_str.length > max_length) {
                        max_length = sub_subs_str.length;
                        maxp = sub_subs_str;
                    }
                }
            }
        }

        return maxp;
    } catch (err) {
        console.log("err : ", err)
    }

}

console.log("running result : ",longestPalindrome(s))