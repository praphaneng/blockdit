const express = require('express');
const app = express();

const service = require('./service');

app.use(express.json())

app.post('/longest-palindromic-substring', function (req, res) {
  try {
    if (!req.body.s) throw "pls send the string with it";

    let s = req.body.s;

    service.isPalindromic(s);
    let longest_palindrome = service.longestPalindrome(s);

    res.send(longest_palindrome);

  } catch (err) {
    res.send(err)
  }
})

app.listen(3000)
console.log('3000 is Running...');