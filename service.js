const isPalindromic = (words) => {
    try {
        if (!words || words == null || words == undefined || words == "") throw "words is empty"

        let rev = [...words].reverse().join("");

        if (words == rev) return result = {
            result: true,
            mes: words == rev,
        }
    } catch (err) {
        console.log(err);
    }
}

const longestPalindrome = (str1) => {
    try {
        let max_length = 0,
            maxp = '';

        for (let i = 0; i < str1.length; i++) {
            let subs = str1.substr(i, str1.length);

            for (let j = subs.length; j >= 0; j--) {
                let sub_subs_str = subs.substr(0, j);
                if (sub_subs_str.length <= 1)
                    continue;

                if (isPalindromic(sub_subs_str).result == true) {
                    if (sub_subs_str.length > max_length) {
                        max_length = sub_subs_str.length;
                        maxp = sub_subs_str;
                    }
                }
            }
        }

        return maxp;
    } catch (err) {
        console.log(err);
    }
}

export {
    isPalindromic,
    longestPalindrome
}